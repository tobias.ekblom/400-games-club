var $j = jQuery.noConflict();
$j(document).ready(function () {
	$j('.games-club-landing-page-widget.game-slider').slick({
		arrows:true,
		centerMode: true,
		centerPadding: '25%',
		slidesToShow: 1,
		accessibility: false,
		autoplay: true,
		dots: true,
		dotsClass: 'games-club-landing-page-widget slick-dots',
		responsive: [
			{
				breakpoint: 965,
				settings: {
					arrows: false,
					infinite: false,
					dots: false,
					centerPadding: '6vw',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$j('.games-club-landing-page-widget.comments').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: false,
		responsive: [
			{
				breakpoint: 960,
				settings: {
					arrows: false,
					dots: false,
					dotsClass: 'games-club-landing-page-widget slick-dots',
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplay:true
				}
			}
		]
	});

});